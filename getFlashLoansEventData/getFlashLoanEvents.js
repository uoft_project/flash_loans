const Web3 = require("web3");
const fileData = require("./aave_lendingpool_contract.js");

const mainnet = "https://mainnet.infura.io/v3/cc6fc2d5012c416eb2f593f4946d9b90"; 
const web3 = new Web3(new Web3.providers.HttpProvider(mainnet));

const loadFlashLoans = async function(contractABI, contractAddress, fileName) {
	const contractInstance = new web3.eth.Contract(contractABI, contractAddress);
	if (contractInstance) {
      let options = {
        fromBlock: 0,
        toBlock: 'latest'
      };
      try {
        // In theory options parameter is optional. In practice an empty array is
        // returned if options is not provided with fromBlock and toBlock set.
		let flashloans = await contractInstance.getPastEvents('FlashLoan', options);
		const fs = require('fs'); 
		fs.writeFile(fileName, JSON.stringify(flashloans), 'utf8', function (err) {
            if (err) {
                console.log("An error occured while writing JSON Object to File.");
                return console.log(err);
            }
            console.log(fileName," file has been saved.");
        }); 
      }
      catch (error) {
        console.log(error);
      }
    }
};

fileData.data.forEach((item) => {
		loadFlashLoans(item.abi, item.address, 'aave_flashloans_history.json');
})

