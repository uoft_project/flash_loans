import os
import subprocess
import sys

if (len(sys.argv) == 3):
    print("Starting scraping of data for the transactions...")
    # Developer Notes: 
    # str(sys.argv[1]) => inputFile name (transactions list)
    # str(sys.argv[2]) => outputFile name (scrapped data)
    startWebScraping = subprocess.run(["node", "getHTMLPage.js", str(sys.argv[1]), str(sys.argv[2])])
    print("The exit code was: %d" % startWebScraping.returncode)
else:
    print("Error: Please enter the file name as the 3rd argument while running the py file")