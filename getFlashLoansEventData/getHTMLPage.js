const axios = require("axios"); //  library to fetch the data from URL
const cheerio = require("cheerio"); //  library to load html page locally and get all elements of HTML page
const fs = require("fs"); //    library to read/write to a particular file
const txnsFileData = require("./flashloan_transactions.json"); //  file contains all the transaction hashes
// const txnsFileData = require("./" + process.argv.slice(2)[0] + ".json");    //  file containes all the transaction hashes
let txHeaderUrl = "https://etherscan.io/tx/"; //  static url header of the transaction on etherscan
data = []; //  store the scrapped data of HTML page

//  function to write the data in a file
let writeInFile = async (fileName, data) => {
    await fs.writeFile(fileName, JSON.stringify(data), "utf8", function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }
        console.log(fileName, " file has been saved.");
    });
};

//  main function  from where web scraping gets started
let startWebScraping = async () => {
    for (var key of Object.keys(txnsFileData.transactionHash)) {
        //  uncomment the following if condition to check few transactions only
        // if (Number(key) < 1){
        //uncomment the following line to generate the scraped data
        await getHTMLPage(txnsFileData.transactionHash[key]);
        // }
    }

    //  writing the scraped data  into the file..
    // const fileName = "scrapedData.json";
    var fileName = process.argv.slice(3)[0] + ".json"
    await writeInFile(fileName, data);

    //  uncomment the following code if you want to generate the file of 'To' ptotocols with their count
    //  fetch the 'To' protocols and their count for all the transactions and writing all the
    //  collective data with 'to_protocol_names; : 'their_count' into the file
    // var toProtocolNameCountData = {};
    // // const txWebpageScrapedData = require("./scrapedData3.json");
    // var toProtocolNamesCountFile = "toProtocolNamesCount.json";
    // for (var key in Object.keys(data)) {
    //     if (data[key].hasOwnProperty("To")) {
    //         if (toProtocolNameCountData[data[key].To] == undefined) {
    //             toProtocolNameCountData[data[key].To] = 1;
    //         } else {
    //             toProtocolNameCountData[data[key].To] += 1;
    //         }
    //     }
    // }
    // writeInFile(toProtocolNamesCountFile, toProtocolNameCountData);

    //  uncomment the following code if you want to get the 'To' protocol name which is maximum number of times used
    // get to 'To' protocol name with the maximum count which means which is used most
    // number of times in the flash_loans transactions
    // var max_count = 0;
    // var to_protocol_max;
    // Object.keys(toProtocolNameCountData).forEach((key) => {
    //     // console.log("coming in for")
    //     // console.log(key[key])
    //     if (Number(toProtocolNameCountData[key]) > Number(max_count)) {
    //         console.log("coming inside if");
    //         max_count = toProtocolNameCountData[key];
    //         to_protocol_max = key;
    //     }
    // });
    // console.log("Final To protocol: " + to_protocol_max);
};

//  get the rendered HTML page using axios library
let getHTMLPage = async (txHash) => {
    txUrl = txHeaderUrl + txHash;
    console.log(txUrl);
    await axios
        .get(txUrl)
        .then((response) => {
            // console.log(response.data);
            getScrapedData(response.data, txHash);
        })
        .catch((error) => {
            console.log(error);
        });
};

scrapedDataArr = [];    //  temporary array to store the data from elements from 'Tokens' transferred section
etherTransferArr = [];  //  temporary array to store the data from elements from from 'To' section for 'Ether' transfers
//  get the scrapped data (webpage elements) for the HTML page fetched using axios library
let getScrapedData = (html, txHash) => {
    // console.log("coming in get Data");
    const $ = cheerio.load(html);

    //  Get the data for 'Ether' Transfers from 'To' section of the etherscan webpage
    $("div.col-md-9 ul[class='list-unstyled'] li[class='media align-items-baseline']").each(
        (i, elem) => {
            console.log("coming in ether transfer section")
            // const groupRegexPattern = /(From )([a-zA-Z : 0-9 \  \.]*)(To )([a-z A-Z : \  0-9 \.]*)( For )([0-9 \. \,]*)(\(\$[0-9 \. \,]*\)|(\ ))(\ )*([0-9 a-z A-Z \.]*)(\([0-9 A-Z a-z \.]{2,10}\))/gm;
            const etherTransferRegex = /(TRANSFER)(\s)*([0-9 \. \, \ ]*)([a-z A-Z 0-9\ ]*)(From\ )(\s)*([a-z A-Z 0-9 \: \ ]*)(To \ )(\s)*([a-z A-Z 0-9 \: \ ]*)/g;
            // if (i == 0) {

            temp = $(elem).text().toString().trim();
            etherTransferArr = etherTransferRegex.exec(temp);   //  getting groups from the data of the elements and storing groups in an array

            if (etherTransferArr != null) {
                data.push({
                    Transaction_Hash: txHash,
                    All_Data: $(elem).text().match(etherTransferRegex),
                    From: etherTransferArr[7].toString().trim(),
                    To: etherTransferArr[10].toString().trim(),
                    Amount: etherTransferArr[3].toString().trim(),
                    Token: etherTransferArr[4].toString().trim(),
                    Amt_USD: '',
                });
            }
            
        }
    );

    //  Get the data from 'Token Transferred' section of the etherscan webpage
    $("div.col-md-9 ul li[class='media align-items-baseline mb-2']").each(
        (i, elem) => {
            const groupRegexPattern = /(From )([a-zA-Z : 0-9 \  \.]*)(To )([a-z A-Z : \  0-9 \.]*)( For )([0-9 \. \,]*)(\(\$[0-9 \. \,]*\)|(\ ))(\ )*([0-9 a-z A-Z \.]*)(\([0-9 A-Z a-z \.]{2,10}\))/gm;

            // if (i == 0) {

            scrapedDataArr = groupRegexPattern.exec($(elem).text());    //  getting groups from the data of the elements and storing groups in an array

            if (scrapedDataArr != null) {
                data.push({
                    Transaction_Hash: txHash,
                    All_Data: $(elem).text().match(groupRegexPattern),
                    From: scrapedDataArr[2],
                    To: scrapedDataArr[4],
                    Amount: scrapedDataArr[6].toString(),
                    Token: scrapedDataArr[11].substr(1, scrapedDataArr[11].length - 2),
                    Amt_USD: scrapedDataArr[7]
                        .toString()
                        .substr(1, scrapedDataArr[7].length - 2),
                });
            }

            // }
        }
    );
    
    console.log(data);
};

startWebScraping(); //  entry point to start the web scraping
